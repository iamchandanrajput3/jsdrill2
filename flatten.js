let result = [];

function flatten(elements) {

    if(!Array.isArray(elements))
    {
        throw new Error("Invalid Argument passed");
    }

    for(let i=0;i<elements.length;i++)
    {
        if(Array.isArray(elements[i]))
        {
            flatten(elements[i]);
        }
        else{
            result.push(elements[i]);
        }
    }
    return result;
}

export default flatten;