import each from "../each.js";

const elements = [1, 2, 3, 4, 5, 5];
let result = [];
try{
    result = each(elements,function(element,index){
        return element+2;
    });
}catch(e){
    console.log(e);
}

console.log(result);