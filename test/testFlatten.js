import flatten from "../flatten.js";

const nestedArray = [1, [2], [[3]], [[[4]]]];
let result = [];

try{
    result = flatten(nestedArray);
} catch(e){
    console.log(e);
}

console.log(result);