import reduce from "../reduce.js";

const elements = [1, 2, 3, 4, 5, 5];
let result;

try{
    result = reduce(elements, function(previousVal, currentVal) {
    return previousVal+currentVal;
});
} catch(e){
    console.log(e);
}

console.log(result);