function map(elements, cb) {

    if(!Array.isArray(elements) || typeof cb !== 'function')
    {
        throw new Error("Invalid Arguments passed");
    }

    let result = [];
    for(let i=0;i<elements.length;i++)
    {
        result.push(cb(elements[i]));
    }
    return result;
}

export default map;
