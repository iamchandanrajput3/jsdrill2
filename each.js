function each(elements, cb) {

    if(!Array.isArray(elements) || typeof cb !== 'function')
    {
        throw new Error("Invalid Arguments passed");
    }

    for(let i=0;i<elements.length;i++)
    {
        elements[i] = cb(elements[i],i);
    }
    return elements;
}

export default each;