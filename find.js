function find(elements, cb) {

    if(!Array.isArray(elements) || typeof cb !== 'function')
    {
        throw new Error("Invalid Arguments passed");
    }

    for(let i=0;i<elements.length;i++)
    {
        let bool = cb(elements[i]);
        if(bool === true)
        {
            return elements[i];
        }
    }
    return undefined;
}

export default find;