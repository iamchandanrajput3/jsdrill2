function filter(elements, cb) {

    if(!Array.isArray(elements) || typeof cb !== 'function')
    {
        throw new Error("Invalid Arguments passed");
    }

    let res = [];
    for(let i=0;i<elements.length;i++)
    {
        let bool = cb(elements[i]);
        if(bool === true){
            res.push(elements[i]);
        }
    }
    return res;
}

export default filter;