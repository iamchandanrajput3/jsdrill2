function reduce(elements, cb, startingValue) {

    if(!Array.isArray(elements) || typeof cb !== 'function')
    {
        throw new Error("Invalid Arguments passed");
    }

    let i = 0;

    if(typeof startingValue === 'undefined')
    {
        startingValue = elements[0];
        i = 1;
    }

    let accumulator = startingValue;

    for(i;i<elements.length;i++)
    {
        accumulator = cb(accumulator,elements[i],i,elements);
    }
    return accumulator;
}

export default reduce;